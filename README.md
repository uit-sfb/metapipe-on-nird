# Metapipe On NIRD

[NQS](https://gitlab.com/uit-sfb/nextflow-queuer) deployment for Metapipe on NIRD.

## Usage

### Deployment

- `kubed -renew nird`
- `export NQS_VERSION=master && for f in k8s/*.yaml; do envsubst < "$f" | kubectl apply --force -f - ; done`

Then the following endpoints should be available:
- NQS server: https://metapipe.metakube.sigma2.no/api
- Mongo express: https://metapipe.metakube.sigma2.no/mongoexpress (protected by a password)

To update the images: `kubectl delete pod <podId>`

### Wordpress plugin update

Download the latest version of [Metapipe Wordpress plugin](https://gitlab.com/uit-sfb/metapipe/-/packages/?type=&orderBy=created_at&sort=desc&search%5B%5D=metapipe_wordpress)
and upload it to the Wordpress instance ([see instructions](https://gitlab.com/uit-sfb/metapipe/-/blob/master/wordpress/plugins/metapipe/README.md)).

Note: You may need to clean the cache for the plugin to take effect.

## Recommended configuration

```
process.cpus = 1
process.memory =  '1 GB '
process.'withName:PreProcessReads'.cpus = 8
process.'withName:PreProcessReads'.memory =  '4 GB'
process.'withLabel:assembly'.ext.slices = 8
process.'withName:TrimmomaticPE'.cpus = 2
process.'withName:Rrnapred'.cpus = 8
process.'withName:Rrnapred'.memory =  '2 GB'
process.'withName:PairReads'.cpus = 12
process.'withName:PairReads'.memory =  '2 GB'
process.'withName:Megahit'.cpus = 24
process.'withName:Megahit'.memory =  '24 GB'
process.'withName:KaijuProc'.cpus = 16
process.'withName:BbWrap'.cpus = 16
process.'withName:BbWrap'.memory =  '8 GB'
process.'withName:BbPileup'.memory =  '8 GB'
process.'withName:Maxbin'.cpus = 2
process.'withName:Maxbin'.memory =  '8 GB'
process.'withName:BbSketch'.memory =  '12 GB'
process.'withLabel:functional_assignment'.ext.slices = 8
process.'withName:DiamondProc'.cpus = 6
process.'withName:DiamondProc'.memory =  '8 GB'
process.'withName:InterproscanProc'.cpus = 6
process.'withName:InterproscanProc'.memory =  '16 GB'
process.'withName:InterproscanProc'.cpus = 6
executor.$k8s.queueSize = 8
params.refdbDir = '/refdb'
params.export = 'files/outputs'
```

and optionally: `docker.runOptions = '--user $(id -u) --pull always'`

## Resources on NIRD

Metapipe runs on TRD.

| Site |  # Nodes | vCPU / node | Mem (GB) / node |
| ---------- | ---------- | ---------- | ---------- |
| TOS | 8 | 64 | 256 |
| TRD | 8 | 80 | 768 |
